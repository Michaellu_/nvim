{
  description = "Michaels Neovim";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";

    flake-utils.url = "github:numtide/flake-utils";

    plugin_neovim-ayu = { url = "github:Shatur/neovim-ayu"; flake = false; };
    plugin_nightfox = { url = "github:EdenEast/nightfox.nvim"; flake = false; };
    plugin_nvim-colorizer = { url = "github:NvChad/nvim-colorizer.lua"; flake = false; };
    plugin_bufferline = { url = "github:akinsho/bufferline.nvim"; flake = false; };
    plugin_lualine = { url = "github:nvim-lualine/lualine.nvim"; flake = false; };
    plugin_plenary = { url = "github:nvim-lua/plenary.nvim"; flake = false; };
    plugin_nvim-ts-autotag = { url = "github:windwp/nvim-ts-autotag"; flake = false; };
    plugin_nvim-treesitter = { url = "github:nvim-treesitter/nvim-treesitter"; flake = false; };
    plugin_nvim-treesitter-textobjects = { url = "github:nvim-treesitter/nvim-treesitter-textobjects"; flake = false; };
    plugin_bufdelete = { url = "github:famiu/bufdelete.nvim"; flake = false; };
    plugin_which-key = { url = "github:folke/which-key.nvim"; flake = false; };
    plugin_nvim-autopairs = { url = "github:windwp/nvim-autopairs"; flake = false; };
    plugin_Comment = { url = "github:numToStr/Comment.nvim"; flake = false; };
    plugin_leap = { url = "github:ggandor/leap.nvim"; flake = false; };
    plugin_nnn = { url = "github:luukvbaal/nnn.nvim"; flake = false; };
    plugin_telescope = { url = "github:nvim-telescope/telescope.nvim"; flake = false; };
    plugin_dressing = { url = "github:stevearc/dressing.nvim"; flake = false; };

    plugin_friendly-snippets = { url = "github:rafamadriz/friendly-snippets"; flake = false; };
    plugin_LuaSnip = { url = "github:L3MON4D3/LuaSnip"; flake = false; };
    plugin_nvim-cmp = { url = "github:hrsh7th/nvim-cmp"; flake = false; };
    plugin_cmp-nvim-lsp = { url = "github:hrsh7th/cmp-nvim-lsp"; flake = false; };
    plugin_nvim-lspconfig = { url = "github:neovim/nvim-lspconfig"; flake = false; };
    plugin_neogen = { url = "github:danymat/neogen"; flake = false; };
    plugin_nvim-jdtls = { url = "github:mfussenegger/nvim-jdtls"; flake = false; };
    plugin_nvim-metals = { url = "github:scalameta/nvim-metals"; flake = false; };
  };

  outputs = { self, nixpkgs, flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pluginOverlay = final: prev:
          let
            inherit (prev.vimUtils) buildVimPluginFrom2Nix;
            treesitterGrammars = prev.tree-sitter.withPlugins (_: prev.tree-sitter.allGrammars);

            # Reduce redundancy by manually getting the plugins from input
            plugins = builtins.filter
              (s: (builtins.match "plugin_.*" s) != null)
              (builtins.attrNames inputs);

            # Add the plugin as a neovim plugin
            buildPlug = name: buildVimPluginFrom2Nix {
              pname = name;
              version = "master";
              src = builtins.getAttr name inputs;
            };
          in {
            # Declare plugin set
            neovimPlugins = builtins.listToAttrs (map
              (plugin: {
                name = plugin;
                value = buildPlug plugin;
              })
            plugins);
          };

        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            pluginOverlay
          ];
        };

        # Basic changable init.vim neovim builder
        builder = { customRC ? "", ... }:
          let
            unwrapped = pkgs.neovim-unwrapped.overrideAttrs (prev: {
              propagatedBuildInputs = with pkgs; [
                # Tools
                valgrind
                lldb_16

                # LSPs
                clang-tools_16
			  ];
            });
          in
            pkgs.wrapNeovim unwrapped {
              viAlias = true;
              vimAlias = true;
              configure = {
                inherit customRC;
                packages.vimPackage = {
                  start = builtins.attrValues pkgs.neovimPlugins;
                  opt = [];
                };
              };
            };

      in rec {
        defaultApp = apps.nvim;
        defaultPackage = packages.neovimDefaultLSP;

        apps.nvim = {
            type = "app";
            program = "${defaultPackage}/bin/nvim";
          };

        # Install using inputs.neovim-flake.packages.x86_64-linux.neovimNoLSP into environment.systemPackages
        packages.neovimNoLSP = builder { customRC = ''
		  luafile ${./init.lua}
		'';};

        # Use with nix run gitlab:Michaellu_/nvim
        packages.neovimDefaultLSP = builder { customRC = ''
          luafile ${./init.lua}
          luafile ${./develop.lua}
        ''; };

        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            packages.neovimDefaultLSP
          ];
        };
      }
    );
}
