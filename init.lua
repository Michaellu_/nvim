--
-- Config for neovim and vscode
--

--
-- Plugins
--

-- Quick Navigation
require('leap').add_default_mappings()

if vim.g.vscode then
	goto exit
end

--
-- General Configuration
--

vim.opt.viewoptions:remove "curdir" -- disable saving current directory with views
vim.opt.shortmess:append { s = true, I = true } -- disable startup message
vim.opt.backspace:append { "nostop" } -- Don't stop backspace at insert
vim.opt.diffopt:append "linematch:60" -- enable linematch diff algorithm

vim.opt.breakindent = true -- Wrap indent to match line start
vim.opt.cmdheight = 0 -- hide command line unless needed
vim.opt.completeopt = { "menuone", "noselect" } -- Options for insert mode completion
vim.opt.cursorline = true -- Highlight the text line of the cursor
vim.opt.expandtab = false -- Enable the use of space in tab
vim.opt.fileencoding = "utf-8" -- File content encoding for the buffer
vim.opt.history = 100 -- Number of commands to remember in a history table
vim.opt.ignorecase = true -- Case insensitive searching
vim.opt.infercase = true -- Infer cases in keyword completion
vim.opt.laststatus = 3 -- globalstatus
vim.opt.linebreak = true -- Wrap lines at 'breakat'
vim.opt.mouse = "" -- Enable mouse support
vim.opt.number = true -- Show numberline
vim.opt.preserveindent = true -- Preserve indent structure as much as possible
vim.opt.pumheight = 10 -- Height of the pop up menu
vim.opt.relativenumber = true -- Show relative numberline
vim.opt.shiftwidth = 4 -- Number of space inserted for indentation
vim.opt.showmode = false -- Disable showing modes in command line
vim.opt.showtabline = 2 -- always display tabline
vim.opt.smartcase = true -- Case sensitivie searching
vim.opt.smartindent = true -- Smarter autoindentation
vim.opt.splitbelow = true -- Splitting a new window below the current one
vim.opt.splitkeep = vim.fn.has "nvim-0.9" == 1 and "screen" or nil -- Maintain code view when splitting
vim.opt.splitright = true -- Splitting a new window at the right of the current one
vim.opt.tabstop = 4 -- Number of space in a tab
vim.opt.termguicolors = true -- Enable 24-bit RGB color in the TUI
vim.opt.timeoutlen = 500 -- Shorten key timeout length a little bit for which-key
vim.opt.undofile = true -- Enable persistent undo
vim.opt.wrap = true -- Disable wrapping of lines longer than the width of window
vim.opt.writebackup = false -- Disable making a backup before overwriting a file
vim.opt.list = true -- Enable seeing tabs and spaces
vim.opt.spelllang = { 'en' } -- Use english dictionary
vim.opt.nrformats = 'alpha,octal,hex,bin' -- Enable ctrl+a/x incrementing and decrementing

vim.g.highlighturl_enabled = true -- highlight URLs by default
vim.g.mapleader = " " -- set leader key

vim.t = { bufs = vim.api.nvim_list_bufs() } -- initialize buffers for the current tab

--
-- Autocmds
--

local augroup = vim.api.nvim_create_augroup
local autocmd = vim.api.nvim_create_autocmd

-- Terminal works as a terminal
autocmd('TermClose', {
	command = 'bdelete! ' .. vim.fn.expand('<abuf>')
})

autocmd('TermOpen', {
	command = 'startinsert'
})

-- Highlight yanking
autocmd("TextYankPost", {
	desc = "Highlight yanked text",
	group = augroup("highlightyank", { clear = true }),
	pattern = "*",
	callback = function() vim.highlight.on_yank() end,
})

autocmd("FileType", {
	desc = "Unlist quickfist buffers",
	group = augroup("unlist_quickfist", { clear = true }),
	pattern = "qf",
	callback = function() vim.opt_local.buflisted = false end,
})

--
-- Plugin Initialization
--

-- Colorscheme
require('ayu').setup {
	overrides = {
		LineNrAbove = { fg = '#51B3EC' },
		LineNrBelow = { fg = '#FB508F' },
	},
}
vim.cmd [[colorscheme ayu]]

-- Enables color hightlights for hex colorcodes
require('colorizer').setup()

-- Bufferline
require('bufferline').setup {
	options = {
		show_buffer_close_icons = false,
		show_close_icon = false,
	}
}

-- Statusline
require('lualine').setup {
	options = {
		icons_enabled = false,
		theme = 'ayu',
		component_separators = '|',
		section_separators = '',
	},
}

-- Show window with all matching keys
require("which-key").setup {
	disable = { filetypes = { "TelescopePrompt" } },
}

-- Automatically place pairs
require("nvim-autopairs").setup {
	check_ts = true,
	ts_config = { java = false },
}

-- Commenter
require('Comment').setup()

-- nnn file browser
require('nnn').setup {}

-- Telescope
require('telescope').setup {}

-- Overwrite select prompt to use telescope
require('dressing').setup {
	input = {
		enabled = false,
	},
	select = {
		backend = { 'telescope', 'builtin' }
	},
}

-- Provides a snippet functionality
require('luasnip.loaders.from_vscode').lazy_load()
require('luasnip.loaders.from_snipmate').lazy_load()
require('luasnip.loaders.from_lua').lazy_load()

-- Provides completion
local cmp = require('cmp')
cmp.setup {
	snippet = {
		expand = function(args)
			require('luasnip').lsp_expand(args.body)
		end,
	},
	mapping = cmp.mapping.preset.insert({
		['<C-k>'] = cmp.mapping.scroll_docs(-4),
		['<C-j>'] = cmp.mapping.scroll_docs(4),
		['<C-Space>'] = cmp.mapping.complete(),
		['<CR>'] = cmp.mapping.confirm {
			select = true,
		},
		['<Tab>'] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			else
				fallback()
			end
		end, { 'i', 's' }),
		['<S-Tab>'] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			else
				fallback()
			end
		end, { 'i', 's' }),
	}),
	sources = cmp.config.sources({
		{ name = 'nvim_lsp' },
		{ name = 'nvim_lsp_signature_help' },
		{ name = 'luasnip' },
		{ name = 'path' },
		{ name = 'buffer' },
	}, { name = 'buffer' }),
}

-- Documentation Tools
require('neogen').setup {
	snippet_engine = "luasnip"
}

--
-- LSPs
--

local capabilities = require('cmp_nvim_lsp').default_capabilities()

vim.keymap.set('n', 'g[', vim.diagnostic.goto_prev, { desc = "Goto previous error/warning" })
vim.keymap.set('n', 'g]', vim.diagnostic.goto_next, { desc = "Goto next error/warning" })
On_attach = function(_, bufnr)
	vim.keymap.set('n', 'gh', vim.lsp.buf.hover, { buffer = bufnr, desc = "Show hover code information" })
	vim.keymap.set('n', 'gH', vim.lsp.buf.code_action, { buffer = bufnr, desc = "Select code action" })
	vim.keymap.set('n', 'gd', vim.lsp.buf.definition, { buffer = bufnr, desc = "Goto definition" })
	vim.keymap.set('n', 'gD', vim.lsp.buf.implementation, { buffer = bufnr, desc = "Goto implementation" })
	vim.keymap.set('n', 'gr', vim.lsp.buf.rename, { buffer = bufnr, desc = "Rename code symbol" })
	vim.keymap.set('n', 'gR', vim.lsp.buf.references, { buffer = bufnr, desc = "Show code references" })
	vim.keymap.set('n', '<a-i>', vim.lsp.buf.format, { buffer = bufnr })
	vim.keymap.set('i', '<c-k>', vim.lsp.buf.signature_help, { buffer = bufnr })

	vim.keymap.set('i', '<c-n>', function()
		require('luasnip').jump(1)
	end, { buffer = bufnr })
	vim.keymap.set('i', '<c-p>', function()
		require('luasnip').jump(-1)
	end, { buffer = bufnr })

	vim.keymap.set('n', '<leader>d', require('neogen').generate, { buffer = bufnr })
end

--
-- Mappings
--

local maps = { i = {}, n = {}, v = {}, t = {} }

-- Standard Operations
-- maps.n["j"] = { "'gj'", expr = true, desc = "Move cursor down" }
-- maps.n["k"] = { "'gk'", expr = true, desc = "Move cursor up" }
maps.n["|"] = { "<cmd>vsplit<cr>", desc = "Vertical Split" }
maps.n["\\"] = { "<cmd>split<cr>", desc = "Horizontal Split" }

-- Manage Buffers
maps.n["<a-h>"] = { "<cmd>bp<cr>", desc = "Move to left buffer or loop" }
maps.n["<a-l>"] = { "<cmd>bn<cr>", desc = "Move to right buffer or loop" }
maps.n["<F3>"] = { "<cmd>Bdelete<cr>", desc = "Close open buffer" }
maps.n["<leader>v"] = { "<cmd>e $MYVIMRC<cr>", desc = "Open init.lua" }

-- Navigate tabs
maps.n["]t"] = { function() vim.cmd.tabnext() end, desc = "Next tab" }
maps.n["[t"] = { function() vim.cmd.tabprevious() end, desc = "Previous tab" }

-- Splits
maps.n["<C-k>"] = { "<cmd>wincmd k<cr>", desc = "Move to top split" }
maps.n["<C-j>"] = { "<cmd>wincmd j<cr>", desc = "Move to bottom split" }
maps.n["<C-h>"] = { "<cmd>wincmd h<cr>", desc = "Move to left split" }
maps.n["<C-l>"] = { "<cmd>wincmd l<cr>", desc = "Move to right split" }
maps.n["<C-Up>"] = { "<cmd>resize -2<CR>", desc = "Resize split up" }
maps.n["<C-Down>"] = { "<cmd>resize +2<CR>", desc = "Resize split down" }
maps.n["<C-Left>"] = { "<cmd>vertical resize -2<CR>", desc = "Resize split left" }
maps.n["<C-Right>"] = { "<cmd>vertical resize +2<CR>", desc = "Resize split right" }

-- Copying
maps.v["<c-c>"] = { '"+y', desc = "Copy selected to clipboard." }

-- Telescope (Searching)
maps.n["<leader>ff"] = { function() require("telescope.builtin").find_files() end, desc = "Find files" }
maps.n["<leader>fF"] = {
	function() require("telescope.builtin").find_files { hidden = true, no_ignore = true } end,
	desc = "Find all files",
}
maps.n["<leader>fw"] = { function() require("telescope.builtin").live_grep() end, desc = "Find words" }
maps.n["<leader>fW"] = {
	function()
		require("telescope.builtin").live_grep {
			additional_args = function(args) return vim.list_extend(args, { "--hidden", "--no-ignore" }) end,
		}
	end,
	desc = "Find words in all files",
}
maps.n["<leader>fb"] = {
	function() require("nnn").toggle("picker") end,
	desc = "nnn file browser"
}
maps.n["<leader>l"] = { function() require("telescope.builtin").lsp_document_symbols() end, desc = "Search symbols" }

-- Terminal
if vim.fn.executable "lazygit" == 1 then
	maps.n["<leader>g"] = { "<cmd>terminal lazygit<cr>", desc = "Terminal lazygit" }
end
if vim.fn.executable "node" == 1 then
	maps.n["<leader>n"] = { "<cmd>terminal node<cr>", desc = "Terminal node" }
end
if vim.fn.executable "gdu" == 1 then
	maps.n["<leader>u"] = { "<cmd>terminal gdu<cr>", desc = "Terminal gdu" }
end
if vim.fn.executable "btm" == 1 then
	maps.n["<leader>b"] = { "<cmd>terminal btm<cr>", desc = "Terminal btm" }
end
if vim.fn.executable "python" == 1 then
	maps.n["<leader>p"] = { "<cmd>terminal python<cr>", desc = "Terminal python" }
end
maps.n["<leader>t"] = { "<cmd>terminal<cr>", desc = "Open a terminal" }

-- Stop search on ESC
maps.n["<esc>"] = { "<cmd>noh<CR>", desc = "Stop search with ESC" }

-- Spelling
maps.n["<leader>z"] = {
	function()
		vim.opt_local.spell = not (vim.opt_local.spell:get())
		print("spell: " .. tostring(vim.opt_local.spell:get()))
	end,
	desc = "Enable spell checking"
}
maps.n["ze"] = {
	function()
		vim.opt.spelllang = { 'en' }
		print("lang: english")
	end,
	desc = "Set spellcheck to english"
}
maps.n["zd"] = {
	function()
		vim.opt.spelllang = { 'de' }
		print("lang: german")
	end,
	desc = "Set spellcheck to german"
}

for mode, body in pairs(maps) do
	-- iterate over each keybinding set in the current mode
	for keymap, options in pairs(body) do
		local cmd = options[1]
		local keymap_opts = vim.tbl_deep_extend("force", {}, options)
		keymap_opts[1] = nil
		vim.keymap.set(mode, keymap, cmd, keymap_opts)
	end
end

::exit::
